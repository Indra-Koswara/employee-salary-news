package com.example.DataEmployes.DTO;

public class AgamaDTO {

	private int idAgama;
	private String namaAgama;
	
	public AgamaDTO(int idAgama, String namaAgama) {
		super();
		this.idAgama = idAgama;
		this.namaAgama = namaAgama;
	}
	public AgamaDTO() {
		super();
	}
	public int getIdAgama() {
		return idAgama;
	}
	public void setIdAgama(int idAgama) {
		this.idAgama = idAgama;
	}
	public String getNamaAgama() {
		return namaAgama;
	}
	public void setNamaAgama(String namaAgama) {
		this.namaAgama = namaAgama;
	}
	
	
}
