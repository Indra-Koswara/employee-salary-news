package com.example.DataEmployes.DTO;

import java.util.Date;



public class LemburBonusDTO {

	private long idLemburBonus;
	private KaryawanDTO karyawan;
	private Date tanggalLemburBonus;
	private int lamaLembur;
	private int variableBonus;
	public LemburBonusDTO(long idLemburBonus, KaryawanDTO karyawan, Date tanggalLemburBonus, int lamaLembur,
			int variableBonus) {
		super();
		this.idLemburBonus = idLemburBonus;
		this.karyawan = karyawan;
		this.tanggalLemburBonus = tanggalLemburBonus;
		this.lamaLembur = lamaLembur;
		this.variableBonus = variableBonus;
	}
	public LemburBonusDTO() {
		super();
	}
	public long getIdLemburBonus() {
		return idLemburBonus;
	}
	public void setIdLemburBonus(long idLemburBonus) {
		this.idLemburBonus = idLemburBonus;
	}
	public KaryawanDTO getKaryawan() {
		return karyawan;
	}
	public void setKaryawan(KaryawanDTO karyawan) {
		this.karyawan = karyawan;
	}
	public Date getTanggalLemburBonus() {
		return tanggalLemburBonus;
	}
	public void setTanggalLemburBonus(Date tanggalLemburBonus) {
		this.tanggalLemburBonus = tanggalLemburBonus;
	}
	public int getLamaLembur() {
		return lamaLembur;
	}
	public void setLamaLembur(int lamaLembur) {
		this.lamaLembur = lamaLembur;
	}
	public int getVariableBonus() {
		return variableBonus;
	}
	public void setVariableBonus(int variableBonus) {
		this.variableBonus = variableBonus;
	}
	
	
}
