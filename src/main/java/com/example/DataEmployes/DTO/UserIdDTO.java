package com.example.DataEmployes.DTO;

public class UserIdDTO {

	private int idUser;
	private String username;
	public UserIdDTO(int idUser, String username) {
		super();
		this.idUser = idUser;
		this.username = username;
	}
	public UserIdDTO() {
		super();
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
