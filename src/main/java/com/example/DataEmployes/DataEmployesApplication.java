package com.example.DataEmployes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataEmployesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataEmployesApplication.class, args);
	}

}
