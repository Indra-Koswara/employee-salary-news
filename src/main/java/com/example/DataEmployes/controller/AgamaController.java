package com.example.DataEmployes.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.DataEmployes.DTO.AgamaDTO;
import com.example.DataEmployes.exception.ResourceNotFoundException;
import com.example.DataEmployes.models.Agama;
import com.example.DataEmployes.repository.AgamaRepository;

@RestController
@RequestMapping("/api")
public class AgamaController {

	@Autowired
	AgamaRepository agamaRepository;
	
	@GetMapping("/agama/read")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Agama> listAgama = (ArrayList<Agama>) agamaRepository.findAll();
		ArrayList<AgamaDTO> listAgamaDTO = new ArrayList<AgamaDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Agama agama : listAgama) {
			AgamaDTO agamaDTO = modelMapper.map(agama, AgamaDTO.class);
			
			listAgamaDTO.add(agamaDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read data agama success");
		result.put("Data", listAgamaDTO);
		return result;
	}
	
	@PostMapping("/agama/create")
	public HashMap<String, Object> createDataAgama (@Valid @RequestBody AgamaDTO agamaDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Agama agamaEntity = modelMapper.map(agamaDTO, Agama.class);
		
		agamaRepository.save(agamaEntity);
		agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create new data agama success");
		result.put("Data", agamaDTO);
		
		return result;
		
	}
	@PutMapping("/agama/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Integer idAgama, @Valid @RequestBody AgamaDTO agamaDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Agama agama = agamaRepository.findById(idAgama).orElseThrow(()-> new ResourceNotFoundException("Agama", "id", idAgama));
		agama = modelMapper.map(agamaDTO, Agama.class);
		agama.setIdAgama(idAgama);
		
		agamaRepository.save(agama);
		agamaDTO = modelMapper.map(agama, AgamaDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update data agama success");
		result.put("Data", agamaDTO);
		return result;
	}
	
	@DeleteMapping("/agama/delete/{id}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "id")Integer idAgama){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		AgamaDTO agamaDTO = new AgamaDTO();
		Agama agamaEntity = agamaRepository.findById(idAgama).orElseThrow(()-> new ResourceNotFoundException("Agama", "id", idAgama));
		agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);
		
		agamaRepository.delete(agamaEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete data agama success");
		result.put("Data", agamaDTO);
		return result;
	}
}
