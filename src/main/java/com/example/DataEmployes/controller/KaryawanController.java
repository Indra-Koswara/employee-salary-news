package com.example.DataEmployes.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.DataEmployes.DTO.KaryawanDTO;
import com.example.DataEmployes.exception.ResourceNotFoundException;
import com.example.DataEmployes.models.Karyawan;
import com.example.DataEmployes.repository.KaryawanRepository;

@RestController
@RequestMapping("/api")
public class KaryawanController {

	@Autowired
	KaryawanRepository karyawanRepository;
	
	@GetMapping("/karyawan/read")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Karyawan> listKaryawan = (ArrayList<Karyawan>) karyawanRepository.findAll();
		ArrayList<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Karyawan karyawan : listKaryawan) {
			KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
			
			listKaryawanDTO.add(karyawanDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read data karyawan success");
		result.put("Data", listKaryawanDTO);
		return result;
	}
	
	@PostMapping("/karyawan/create")
	public HashMap<String, Object> createDataAgama (@Valid @RequestBody KaryawanDTO karyawanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Karyawan karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class);
		
		karyawanRepository.save(karyawanEntity);
		karyawanDTO = modelMapper.map(karyawanEntity, KaryawanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create new data karyawan success");
		result.put("Data", karyawanDTO);
		
		return result;
		
	}
	@PutMapping("/karyawan/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Integer idKaryawan, @Valid @RequestBody KaryawanDTO karyawanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Karyawan karyawan = karyawanRepository.findById(idKaryawan).orElseThrow(()-> new ResourceNotFoundException("Karyawan", "id", idKaryawan));
		karyawan = modelMapper.map(karyawanDTO, Karyawan.class);
		karyawan.setIdKaryawan(idKaryawan);
		
		karyawanRepository.save(karyawan);
		karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update data karyawan success");
		result.put("Data", karyawanDTO);
		return result;
	}
	
	@DeleteMapping("/karyawan/delete/{id}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "id")Integer idKaryawan){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		KaryawanDTO karyawanDTO = new KaryawanDTO();
		Karyawan karyawanEntity = karyawanRepository.findById(idKaryawan).orElseThrow(()-> new ResourceNotFoundException("Karyawan", "id", idKaryawan));
		karyawanDTO = modelMapper.map(karyawanEntity, KaryawanDTO.class);
		
		karyawanRepository.delete(karyawanEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete data karyawan success");
		result.put("Data", karyawanDTO);
		return result;
	}
}
