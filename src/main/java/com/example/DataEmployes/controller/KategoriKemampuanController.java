package com.example.DataEmployes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.DataEmployes.DTO.KategoriKemampuanDTO;
import com.example.DataEmployes.exception.ResourceNotFoundException;
import com.example.DataEmployes.models.KategoriKemampuan;
import com.example.DataEmployes.repository.KategoriKemampuanRepository;

@RestController
@RequestMapping("/api")
public class KategoriKemampuanController {

	@Autowired
	KategoriKemampuanRepository kategoriKemampuanRepository;
	
	@GetMapping("kategori_kemampuan/read")
	public HashMap<String, Object> getAllData () {
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		ArrayList<KategoriKemampuan> listKategoriKemampuan = (ArrayList<KategoriKemampuan>) kategoriKemampuanRepository.findAll();
		ArrayList<KategoriKemampuanDTO> listKategoriDTO = new ArrayList<KategoriKemampuanDTO>();
		
		for (KategoriKemampuan kK : listKategoriKemampuan) {
			KategoriKemampuanDTO kKDTO = modelMapper.map(kK, KategoriKemampuanDTO.class);
			
			listKategoriDTO.add(kKDTO);
		}
			result.put("Status", 200);
			result.put("Message", "Read data success");
			result.put("Data", listKategoriDTO);
		return result;
	}
	
	@PostMapping("/kategori_kemampuan/create")
		public HashMap<String, Object> createDataKategori (@Valid @RequestBody KategoriKemampuanDTO kKDTO){
			HashMap<String, Object> result = new HashMap<String, Object>();
			ModelMapper modelMapper = new ModelMapper();
			KategoriKemampuan kKEntity = modelMapper.map(kKDTO, KategoriKemampuan.class);
			
			kategoriKemampuanRepository.save(kKEntity);
			kKDTO = modelMapper.map(kKEntity, KategoriKemampuanDTO.class);
			
			result.put("Status", 200);
			result.put("Message", "Create new data kategori kemampuan success");
			result.put("Data", kKDTO);
			
			return result;
		}
	@PutMapping("/kategori_kemampuan/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Integer idKategori, @Valid @RequestBody KategoriKemampuanDTO kKDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		KategoriKemampuan kKEntity = kategoriKemampuanRepository.findById(idKategori).orElseThrow(()-> new ResourceNotFoundException("KategoriKemampuan", "id", idKategori));
		kKEntity = modelMapper.map(kKDTO, KategoriKemampuan.class);
		kKEntity.setIdKategori(idKategori);
		
		kategoriKemampuanRepository.save(kKEntity);
		kKDTO = modelMapper.map(kKEntity, KategoriKemampuanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update data kategori kemampuan success");
		result.put("Data", kKDTO);
		return result;
	}
	
	@DeleteMapping("/kategori_kemampuan/delete/{id}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "id")Integer idKategori){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		KategoriKemampuanDTO kKDTO = new KategoriKemampuanDTO();
		KategoriKemampuan kKEntity = kategoriKemampuanRepository.findById(idKategori).orElseThrow(()-> new ResourceNotFoundException("KategoriKemampuan", "id", idKategori));
		kKDTO = modelMapper.map(kKEntity, KategoriKemampuanDTO.class);
		
		kategoriKemampuanRepository.delete(kKEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete data kategori kemampuan success");
		result.put("Data", kKDTO);
		return result;
	}
}
