package com.example.DataEmployes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.DataEmployes.DTO.KemampuanDTO;
import com.example.DataEmployes.exception.ResourceNotFoundException;
import com.example.DataEmployes.models.Kemampuan;
import com.example.DataEmployes.repository.KemampuanRepository;

@RestController
@RequestMapping("/api")
public class KemampuanController {

	@Autowired
	KemampuanRepository kemampuanRepository;
	
	@GetMapping("/kemampuan/read")
	public HashMap<String, Object> getAllData () {
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		ArrayList<Kemampuan> listKemampuan = (ArrayList<Kemampuan>) kemampuanRepository.findAll();
		ArrayList<KemampuanDTO> listKemampuanDTO = new ArrayList<KemampuanDTO>();
		
			for(Kemampuan kemampuan : listKemampuan) {
				KemampuanDTO kemampuanDTO = modelMapper.map(kemampuan, KemampuanDTO.class);
				listKemampuanDTO.add(kemampuanDTO);
			}
			result.put("Status", 200);
			result.put("Message", "Read data success");
			result.put("Data", listKemampuanDTO);
		return result;
	}
	@PostMapping("/kemampuan/create")
	public HashMap<String, Object> createDataKategori (@Valid @RequestBody KemampuanDTO kemampuanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Kemampuan kemampuan = modelMapper.map(kemampuanDTO, Kemampuan.class);
		
		kemampuanRepository.save(kemampuan);
		kemampuanDTO = modelMapper.map(kemampuan, KemampuanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create new data kemampuan success");
		result.put("Data", kemampuanDTO);
		
		return result;
	}
	@PutMapping("/kemampuan/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Integer idKemampuan, @Valid @RequestBody KemampuanDTO kemampuanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Kemampuan kemampuan = kemampuanRepository.findById(idKemampuan).orElseThrow(()-> new ResourceNotFoundException("Kemampuan", "id", idKemampuan));
		kemampuan = modelMapper.map(kemampuanDTO, Kemampuan.class);
		kemampuan.setIdKemampuan(idKemampuan);
		
		kemampuanRepository.save(kemampuan);
		kemampuanDTO = modelMapper.map(kemampuan, KemampuanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update data kemampuan success");
		result.put("Data", kemampuanDTO);
		return result;
	}
	
	@DeleteMapping("/kemampuan/delete/{id}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "id")Integer idKemampuan){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		KemampuanDTO kemampuanDTO = new KemampuanDTO();
		Kemampuan kemampuanEntity = kemampuanRepository.findById(idKemampuan).orElseThrow(()-> new ResourceNotFoundException("Kemampuan", "id", idKemampuan));
		kemampuanDTO = modelMapper.map(kemampuanEntity, KemampuanDTO.class);
		
		kemampuanRepository.delete(kemampuanEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete data kemampuan success");
		result.put("Data", kemampuanDTO);
		return result;
	}
}
