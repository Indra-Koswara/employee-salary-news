package com.example.DataEmployes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.DataEmployes.DTO.PenempatanDTO;
import com.example.DataEmployes.exception.ResourceNotFoundException;
import com.example.DataEmployes.models.Penempatan;
import com.example.DataEmployes.repository.PenempatanRepository;

@RestController
@RequestMapping("/api")
public class PenempatanController {

	@Autowired
	PenempatanRepository penempatanRepository;
	
	@GetMapping("/penempatan/read")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Penempatan> listPenempatan = (ArrayList<Penempatan>) penempatanRepository.findAll();
		ArrayList<PenempatanDTO> listPenempatanDTO = new ArrayList<PenempatanDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Penempatan penempatan : listPenempatan) {
			PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
			
			listPenempatanDTO.add(penempatanDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read data penempatan success");
		result.put("Data", listPenempatanDTO);
		return result;
	}
	
	@PostMapping("/penempatan/create")
	public HashMap<String, Object> createDataAgama (@Valid @RequestBody PenempatanDTO penempatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Penempatan penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);
		
		penempatanRepository.save(penempatanEntity);
		penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create new data penempatan success");
		result.put("Data", penempatanDTO);
		
		return result;
		
	}
	@PutMapping("/penempatan/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Integer idPenempatan, @Valid @RequestBody PenempatanDTO penempatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Penempatan penempatan = penempatanRepository.findById(idPenempatan).orElseThrow(()-> new ResourceNotFoundException("Penempatan", "id", idPenempatan));
		penempatan = modelMapper.map(penempatanDTO, Penempatan.class);
		penempatan.setIdPenempatan(idPenempatan);
		
		penempatanRepository.save(penempatan);
		penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update data penempatan success");
		result.put("Data", penempatanDTO);
		return result;
	}
	
	@DeleteMapping("/penempatan/delete/{id}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "id")Integer idPenempatan){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		PenempatanDTO penempatanDTO = new PenempatanDTO();
		Penempatan penempatanEntity = penempatanRepository.findById(idPenempatan).orElseThrow(()-> new ResourceNotFoundException("Penempatan", "id", idPenempatan));
		penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);
		
		penempatanRepository.delete(penempatanEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete data penempatan success");
		result.put("Data", penempatanDTO);
		return result;
	}
}
