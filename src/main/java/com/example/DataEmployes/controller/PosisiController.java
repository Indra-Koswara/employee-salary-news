package com.example.DataEmployes.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.DataEmployes.DTO.PosisiDTO;
import com.example.DataEmployes.exception.ResourceNotFoundException;
import com.example.DataEmployes.models.Posisi;
import com.example.DataEmployes.repository.PosisiRespository;

@RestController
@RequestMapping("/api")
public class PosisiController {

	@Autowired
	PosisiRespository posisiRespository;
	
	@GetMapping("/posisi/read")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object>  result = new HashMap<String, Object>();
		ArrayList<Posisi> listPosisi = (ArrayList<Posisi>) posisiRespository.findAll();
		ArrayList<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Posisi posisi : listPosisi) {
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
			
			listPosisiDTO.add(posisiDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read data posisi success");
		result.put("Data", listPosisiDTO);
		return result;
	}
	
	@PostMapping("/posisi/create")
	public HashMap<String, Object> createDataAgama (@Valid @RequestBody PosisiDTO posisiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Posisi posisiEntity = modelMapper.map(posisiDTO, Posisi.class);
		
		posisiRespository.save(posisiEntity);
		posisiDTO = modelMapper.map(posisiEntity, PosisiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create new data posisi success");
		result.put("Data", posisiDTO);
		
		return result;
		
	}
	@PutMapping("/posisi/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Integer idPosisi, @Valid @RequestBody PosisiDTO posisiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Posisi posisi = posisiRespository.findById(idPosisi).orElseThrow(()-> new ResourceNotFoundException("Posisi", "id", idPosisi));
		posisi = modelMapper.map(posisiDTO, Posisi.class);
		posisi.setIdPosisi(idPosisi);
		
		posisiRespository.save(posisi);
		posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update data posisi success");
		result.put("Data", posisiDTO);
		return result;
	}
	
	@DeleteMapping("/posisi/delete/{id}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "id")Integer idPosisi){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		PosisiDTO posisiDTO = new PosisiDTO();
		Posisi posisiEntity = posisiRespository.findById(idPosisi).orElseThrow(()-> new ResourceNotFoundException("Posisi", "id", idPosisi));
		posisiDTO = modelMapper.map(posisiEntity, PosisiDTO.class);
		
		posisiRespository.delete(posisiEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete data posisi success");
		result.put("Data", posisiDTO);
		return result;
	}
}
