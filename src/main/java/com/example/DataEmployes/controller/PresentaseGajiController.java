package com.example.DataEmployes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.DataEmployes.DTO.PresentaseGajiDTO;
import com.example.DataEmployes.exception.ResourceNotFoundException;
import com.example.DataEmployes.models.PresentaseGaji;
import com.example.DataEmployes.repository.PresentaseGajiRepository;

@RestController
@RequestMapping("/api")
public class PresentaseGajiController {

	@Autowired
	PresentaseGajiRepository presentaseGajiRepository;
	
	@GetMapping("/presentase_gaji/read")
	public HashMap<String, Object> getallData (){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<PresentaseGaji> listData = (ArrayList<PresentaseGaji>) presentaseGajiRepository.findAll();
		ArrayList<PresentaseGajiDTO> listDataDTO = new ArrayList<PresentaseGajiDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (PresentaseGaji pg : listData) {
			PresentaseGajiDTO pgDTO = modelMapper.map(pg, PresentaseGajiDTO.class);
			
			listDataDTO.add(pgDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read data presentase gaji success");
		result.put("Data", listDataDTO);
		return result;
	}
	@PostMapping("/presentase_gaji/create")
	public HashMap<String, Object> createDataAgama (@Valid @RequestBody PresentaseGajiDTO pgDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		PresentaseGaji pgEntity = modelMapper.map(pgDTO, PresentaseGaji.class);
		
		presentaseGajiRepository.save(pgEntity);
		pgDTO = modelMapper.map(pgEntity, PresentaseGajiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create new data presentase gaji success");
		result.put("Data", pgDTO);
		
		return result;
		
	}
	@PutMapping("/presentase_gaji/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Integer idPresentaseGaji, @Valid @RequestBody PresentaseGajiDTO pgDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		PresentaseGaji pgEntity = presentaseGajiRepository.findById(idPresentaseGaji).orElseThrow(()-> new ResourceNotFoundException("PresentaseGaji", "id", idPresentaseGaji));
		pgEntity = modelMapper.map(pgDTO, PresentaseGaji.class);
		pgEntity.setIdPresentaseGaji(idPresentaseGaji);
		
		presentaseGajiRepository.save(pgEntity);
		pgDTO = modelMapper.map(pgEntity, PresentaseGajiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update data presentase gaji success");
		result.put("Data", pgDTO);
		return result;
	}
	
	@DeleteMapping("/presentase_gaji/delete/{id}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "id")Integer idPresentaseGaji){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		PresentaseGajiDTO pgDTO = new PresentaseGajiDTO();
		PresentaseGaji pgEntity = presentaseGajiRepository.findById(idPresentaseGaji).orElseThrow(()-> new ResourceNotFoundException("PresentaseGaji", "id", idPresentaseGaji));
		pgDTO = modelMapper.map(pgEntity, PresentaseGajiDTO.class);
		
		presentaseGajiRepository.delete(pgEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete data presentase gaji success");
		result.put("Data", pgDTO);
		return result;
	}
}
