package com.example.DataEmployes.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.DataEmployes.DTO.TingkatanDTO;
import com.example.DataEmployes.exception.ResourceNotFoundException;
import com.example.DataEmployes.models.Tingkatan;
import com.example.DataEmployes.repository.TingkatanRepository;

@RestController
@RequestMapping("/api")
public class TingkatanController {

	@Autowired
	TingkatanRepository tingkatanRepository;
	
	@GetMapping("/tingkatan/read")
	public HashMap<String, Object> getAllData () {
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Tingkatan> listTingkatan =(ArrayList<Tingkatan>) tingkatanRepository.findAll();
		ArrayList<TingkatanDTO> listTingkatanDTO = new ArrayList<TingkatanDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Tingkatan tingkatan : listTingkatan) {
			TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
			
			listTingkatanDTO.add(tingkatanDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read data tingkatan success");
		result.put("Data", listTingkatanDTO);
		return result;
	}
	
	@PostMapping("/tingkatan/create")
	public HashMap<String, Object> createDataAgama (@Valid @RequestBody TingkatanDTO tingkatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Tingkatan tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);
		
		tingkatanRepository.save(tingkatanEntity);
		tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create new data tingkatan success");
		result.put("Data", tingkatanDTO);
		
		return result;
		
	}
	@PutMapping("/tingkatan/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Integer idTingkatan, @Valid @RequestBody TingkatanDTO tingkatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Tingkatan tingkatan = tingkatanRepository.findById(idTingkatan).orElseThrow(()-> new ResourceNotFoundException("Tingkatan", "id", idTingkatan));
		tingkatan = modelMapper.map(tingkatanDTO, Tingkatan.class);
		tingkatan.setIdTingkatan(idTingkatan);
		
		tingkatanRepository.save(tingkatan);
		tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update data tingkatan success");
		result.put("Data", tingkatanDTO);
		return result;
	}
	
	@DeleteMapping("/tingkatan/delete/{id}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "id")Integer idTingkatan){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		TingkatanDTO tingkatanDTO = new TingkatanDTO();
		Tingkatan tingkatanEntity = tingkatanRepository.findById(idTingkatan).orElseThrow(()-> new ResourceNotFoundException("Tingkatan", "id", idTingkatan));
		tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);
		
		tingkatanRepository.delete(tingkatanEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete data tingkatan success");
		result.put("Data", tingkatanDTO);
		return result;
	}
}
