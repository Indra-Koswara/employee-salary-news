package com.example.DataEmployes.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.DataEmployes.DTO.TunjanganPegawaiDTO;
import com.example.DataEmployes.exception.ResourceNotFoundException;
import com.example.DataEmployes.models.TunjanganPegawai;
import com.example.DataEmployes.repository.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api")
public class TunjanganPegawaiController {

	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	@GetMapping("/tunjangan_pegawai/read")
	public HashMap<String, Object> getallData (){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<TunjanganPegawai> listData = (ArrayList<TunjanganPegawai>) tunjanganPegawaiRepository.findAll();
		ArrayList<TunjanganPegawaiDTO> listDataDTO = new ArrayList<TunjanganPegawaiDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (TunjanganPegawai tp : listData) {
			TunjanganPegawaiDTO tpDTO = modelMapper.map(tp, TunjanganPegawaiDTO.class);
			
			listDataDTO.add(tpDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read data tunjangan pegawai success");
		result.put("Data", listDataDTO);
		return result;
	}
	@PostMapping("/tunjangan_pegawai/create")
	public HashMap<String, Object> createDataAgama (@Valid @RequestBody TunjanganPegawaiDTO tpDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		TunjanganPegawai tpEntity = modelMapper.map(tpDTO, TunjanganPegawai.class);
		
		tunjanganPegawaiRepository.save(tpEntity);
		tpDTO = modelMapper.map(tpEntity, TunjanganPegawaiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create new data tunjangan pegawai success");
		result.put("Data", tpDTO);
		
		return result;
		
	}
	@PutMapping("/tunjangan_pegawai/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Integer idTunjanganPegawai, @Valid @RequestBody TunjanganPegawaiDTO tpDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		TunjanganPegawai tpEntity = tunjanganPegawaiRepository.findById(idTunjanganPegawai).orElseThrow(()-> new ResourceNotFoundException("TunjanganPegawai", "id", idTunjanganPegawai));
		tpEntity = modelMapper.map(tpDTO, TunjanganPegawai.class);
		tpEntity.setIdTunjanganPegawai(idTunjanganPegawai);
		
		tunjanganPegawaiRepository.save(tpEntity);
		tpDTO = modelMapper.map(tpEntity, TunjanganPegawaiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update data tunjangan pegawai success");
		result.put("Data", tpDTO);
		return result;
	}
	
	@DeleteMapping("/tunjangan_pegawai/delete/{id}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "id")Integer idTunjanganPegawai){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		TunjanganPegawaiDTO tpDTO = new TunjanganPegawaiDTO();
		TunjanganPegawai tpEntity = tunjanganPegawaiRepository.findById(idTunjanganPegawai).orElseThrow(()-> new ResourceNotFoundException("TunjanganPegawai", "id", idTunjanganPegawai));
		tpDTO = modelMapper.map(tpEntity, TunjanganPegawaiDTO.class);
		
		tunjanganPegawaiRepository.delete(tpEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete data tunjangan pegawai success");
		result.put("Data", tpDTO);
		return result;
	}
}
