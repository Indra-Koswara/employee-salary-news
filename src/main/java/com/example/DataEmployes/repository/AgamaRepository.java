package com.example.DataEmployes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.DataEmployes.models.Agama;
@Repository
public interface AgamaRepository extends JpaRepository<Agama, Integer>{

}
