package com.example.DataEmployes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.DataEmployes.models.Karyawan;
@Repository
public interface KaryawanRepository extends JpaRepository<Karyawan, Integer>{

}
