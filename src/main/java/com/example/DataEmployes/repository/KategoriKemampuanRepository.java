package com.example.DataEmployes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.DataEmployes.models.KategoriKemampuan;

@Repository
public interface KategoriKemampuanRepository extends JpaRepository<KategoriKemampuan, Integer>{

}
