package com.example.DataEmployes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.DataEmployes.models.Kemampuan;

@Repository
public interface KemampuanRepository extends JpaRepository<Kemampuan, Integer>{

}
