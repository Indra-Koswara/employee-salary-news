package com.example.DataEmployes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.DataEmployes.models.Posisi;
@Repository
public interface PosisiRespository extends JpaRepository<Posisi, Integer>{

}
