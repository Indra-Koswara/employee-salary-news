package com.example.DataEmployes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.DataEmployes.models.PresentaseGaji;
@Repository
public interface PresentaseGajiRepository extends JpaRepository<PresentaseGaji, Integer>{

}
