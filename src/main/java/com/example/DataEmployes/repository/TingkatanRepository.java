package com.example.DataEmployes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.DataEmployes.models.Tingkatan;
@Repository
public interface TingkatanRepository extends JpaRepository<Tingkatan, Integer>{

}
