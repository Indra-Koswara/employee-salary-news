package com.example.DataEmployes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.DataEmployes.models.TunjanganPegawai;
@Repository
public interface TunjanganPegawaiRepository extends JpaRepository<TunjanganPegawai, Integer>{

}
